Views Mailmerge Field 8.x-1.x
-----------------------------

### About this Module

The Views Mailmerge Field module provides a global Views field plugin.

The primary use case for this module is to provide a mailmerge facility.

Mailmerge tokens of the form [key] are replaced by the matching value.
This can be useful for presenting forms that need to be viewed or printed
based on text in a node body and fields from a view. For example, a Webform
can be used for data entry and this field can be used to present that
information based on a node body that includes tokens.

The field can also be used as a global Views text field with token subtitution
support.


### Installing and using the module

1. Copy/upload the module to the modules directory of your Drupal
   installation.

2. Enable the module and desired sub-modules in 'Extend'.
   (/admin/modules)

3. Create or edit a View

4. Make sure the fields to be merged and the field that has the mailmerge
   tokens are defined first. Normally these fields will not be displayed.

5. Add a Global category Mailmerge field.

6. Enter the desired text in the Text field. This can include any replacement
   patterns.
   Typically this is a field defined earlier in the view's field list.
   For example, this might be {{ body }} for a node body field.

7. Define the Mailmerge field tokens. This would typically look like this:

    [text1]|This is a constant
    [site_name]|[site:name]
    [node:id]|{{ nid }}
    [name]|{{ first_name }} {{ last_name }}

   This defines four substitutions. The first is a constant. The second
   uses the global token replacement. The third includes a Views' field value.
   The final one combines two Views fields.

   The tokens in the Text field would be [text1],[site_name],[node:id],[name].

### Considerations

Typically a single Mailmerge field is used. If there are multiple fields and
they need to share the same substitution list then copy the Mailmerge field to
each field definition.

Keys are case and space sensitive and the key consists of all characters to the
left of the vertical bar, '|'.

In general, do not include spaces and use only lower case letters.
Be consistent in the key definitions. For example, use square brackets around
all keys. A format using double square brackets could also be used as in:

    [[ text1 ]]|This is a constant
    [[ site_name ]]|[site:name]
    [[ node:id ]]|{{ nid }}
    [[ name ]]|{{ first_name }} {{ last_name }}

Note, in this case, spaces are significant. If [[text1]] is in the text then
it will not match the first entry above so [[text1]] will remain in the result.

Multiple line substitution text can be created by using Custom text or another
Mailmerge Field to define the content that is then used with a matching key.

Views field substitutions of the form {{ field_name }} will not occur in the
text generated from the main text field. Only mailmerge [key] tokens will be
processed.

Unmatched tokens WILL NOT be replaced by blank space.

Wild card keys are not supported.
