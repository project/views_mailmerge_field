<?php

namespace Drupal\views_mailmerge_field\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Render\ViewsRenderPipelineMarkup;
use Drupal\views\ResultRow;

/**
 * A handler to provide a mailmerge field.
 *
 * The field replaces mailmerge tokens of the form [key] in the text field.
 *
 * Note, the square brackets are a convention. Any text can be used as a key.
 *
 * The text field can include replacement field substitutions
 * of the form {{ field }} as well as global tokens like [site:name].
 * The mailmerge tokens are defined in Mailmerge Fields.
 * Each line is of the form key|replacement
 * The replacement can contain {{ field }} and [global_token] strings.
 * Typically the mailmerge tokens look like:
 *
 *       [key]|{{ field }}
 *
 * And the text field is also of the form:
 *
 *       {{ field }}
 *
 * Mailmerge key tokens are used so that fields in the view
 * can be rearranged without having to modify the main text field.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("mailmerge_field")
 */
class ViewsMailMergeField extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function usesGroupBy() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Do nothing -- to override the parent query.
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['mailmerge_fields'] = ['default' => ''];

    // Override the alter text option to always alter the text.
    $options['alter']['contains']['alter_text'] = ['default' => TRUE];
    $options['hide_alter_empty'] = ['default' => FALSE];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $form['mailmerge_fields'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Mailmerge Fields'),
      '#default_value' => $this->options['mailmerge_fields'],
      '#description' => $this->t('Each line defines a mailmerge substitution with a format <b>key|value</b>. Tokens of the form [key] in the text field above will have these tokens replaced by the matching value. The key should not contain spaces. The value may contain replacement patterns of the for {{ field_name }}. The replacement values may also contain global tokens like [site:name].'),
    ];

    // Remove the checkbox.
    unset($form['alter']['alter_text']);
    unset($form['alter']['text']['#states']);
    unset($form['alter']['help']['#states']);
    $form['#pre_render'][] = [$this, 'preRenderCustomForm'];
  }

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks() {
    $callbacks = parent::trustedCallbacks();
    $callbacks[] = 'preRenderCustomForm';
    return $callbacks;
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    // Return the text, so the code never thinks the value is empty.
    return ViewsRenderPipelineMarkup::create(Xss::filterAdmin($this->options['alter']['text']));
  }

  /**
   * Prerender function to move the textarea to the top of a form.
   *
   * @param array $form
   *   The form build array.
   *
   * @return array
   *   The modified form build array.
   */
  public function preRenderCustomForm(array $form) {
    $form['text'] = $form['alter']['text'];
    $form['help'] = $form['alter']['help'];
    unset($form['alter']['text']);
    unset($form['alter']['help']);

    return $form;
  }

  /**
   * Render this field as user-defined altered text.
   */
  protected function renderAltered($alter, $tokens) {
    $token_service = \Drupal::token();

    $search = [];
    $replace = [];
    foreach (explode("\n", $this->options['mailmerge_fields']) as $line) {
      list($key, $value) = explode('|', $line, 2);
      $search[] = "$key";
      $replace[] = $token_service->replace($this->viewsTokenReplace($value, $tokens));
    }
    return str_replace($search, $replace, $token_service->replace($this->viewsTokenReplace($alter['text'], $tokens)));
  }

}
